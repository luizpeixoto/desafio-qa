Funcionalidade: Como usuário quero gravar uma mensagem de áudio e enviar para um contato

Descrição e contexto da funcionalidade:
	
Atualmente vemos uma necessidade dos usuários em poder enviar mensagens de audio para outros contatos.
Deverá ser inserido um ícone no formato de um microfone ao lado do botão de tirar uma foto.
O novo botão deverá ser separado por um e pontos, um sobre o outro, (imagem em anexo).
Ao pressionar o novo botão o usuário poderá gravar audios para um contato desejado.
Ao largar o botão, automaticamente o áudio gravado será processado e enviado.
É possivel ouvir um áudio que foi gravado e enviado para um contato.
O áudio gravado não deverá ser enviado para a biblioteca de mídias do aplicativo.
Caso o áudio seja grande, é possivel clicar e cancelar o envio. (tamanho áudio é diretamente proporcional ao tempo de processamento)
Não há limite de tempo para os áudios.
Não há limite para envios em sequência para os áudios.
Os áudios deverão utilizar a ferramenta de gravação do próprio celular (32kbps)
É possível intercalar mensagens de voz, mensagens de texto e imagens.
É possível encaminhar um áudio gravado ou recebido para um contato ou um grupo.
Ao segurar o botão do microfone, um som deverá ser emitido.
Ao soltar o botão do microfone, um novo som diferente do de gravação também deverá ser emitido.
Ambos os sons de início da gravação quanto o de fim da gravação, não deverão ser enviados para quem recebe o áudio.
Quando o usuário segurar o botão com mais de 1 segundo, uma mensagem seguindo o seguinte padrão será exibida: 
	1. com ícone de um microfone na cor vermelha, piscando
	2. mensagem de "desliza/cancela"
	3. contador crescente com o tempo decorrido 
Se o usuário segurar o botão e largar com menos de 1 segundo, uma mensagem no formato de um alerta deverá ser exibido com a seguinte mensagem “pressione para gravar, solte para enviar”. 
Se durante a gravação, o usuário deslizar o dedo para esquerda, na direção de "desliza/cancela" a ação de gravação deverá ser cancelada.


******************************************************************************************************************************************
******************************************************************************************************************************************
******************************************************************************************************************************************


Cenário 1: Visualizar novo botão de microfone

Dado que acesso a página de diálogo de conversar
Quando visualizo a parte inferior da tela
Então vejo o ícone do microfone ao lado do botão de enviar fotos 
    E vejo três pontos, um sobre o outro sendo utilizado para separar os dois ícones 

******************************************************************************************************************************************
******************************************************************************************************************************************

Cenário 2: Gravar um mensagem de voz para um contato

Dado que estou na página de diálogo de conversas de um contato
Quando aperto o botão do microfone
    E seguro por 5 segundos 
    E durante este tempo corrido de 5 segundo falo “Hello World”
    E em seguida deixo de pressionar o ícone
Então visualizo que um retângulo é exibido
    E dentro do retângulo a foto do contato contendo o tempo de gravação
    E logo ao lado um ícone de play
    E a hora que a mensagem foi gravada
	E o a flag de mensagem enviada

******************************************************************************************************************************************
******************************************************************************************************************************************

Cenário 3: Ouvir uma mensagem de voz gravada e enviada 

Dado que estou na página de diálogo de conversas
	E possuo um áudio enviado para um contato
Quando pressiono o botão de play exibido na mensagem de áudio enviada
Então ouço o áudio gravado

******************************************************************************************************************************************
******************************************************************************************************************************************

Cenário 4: Verificar envio de áudio gravado para a biblioteca de mídias do contato

Dado que estou na página de diálogo de conversas
	E possuo um áudio enviado para um contato
Quando clico no nome do contato exibido na parte superior do aplicativo
	E em seguida clico em “Mídia, Links e Docs”
Então verifico que o áudio gravado não foi enviado para a biblioteca de mídias
 
******************************************************************************************************************************************
******************************************************************************************************************************************

Cenário 5: Verificar cancelamento de envio de áudio ápos o envio da mensagem

Dado que acabei de gravar um áudio com mais de 1 minuto
	E visualizo a barra de status de processamento do áudio
    E dentro do retângulo a foto do contato contendo o tempo de gravação
    E logo ao lado um ícone de contendo um “X”
Quando pressiono o “X”
Então verifico que a barra de status de processamento do áudio não está mais sendo exibida
	E ao lado direito do caixa do áudio foi inserido o símbolo de erro padrão

******************************************************************************************************************************************
******************************************************************************************************************************************

Cenário 6: Verificar cancelamento de envio de áudio durante a gravação da mensagem

Dado que estou gravando um áudio com mais de 1 minuto
	E visualizo a mensagem de “deslizar cancela”
Quando deslizo o dedo da direita para a esquerda em direção da mensagem
Então verifico que a gravação 

******************************************************************************************************************************************
******************************************************************************************************************************************

Cenário 7: Verificar envio em sequência de áudios durante um diálogo

Dado que estou na página de diálogo de conversas
Quando gravo três áudios consecutivos para um contato
Então verifico três retângulos sendo exibidos de forma consecutiva

******************************************************************************************************************************************
******************************************************************************************************************************************

Cenário 8: Verificar envio em sequência de um áudio, uma imagem e uma mensagem de texto

Dado que estou na página de diálogo de conversas
Quando gravo um áudio para um contato
	E em seguida envio uma imagem
	E em seguida envio uma mensagem de texto
Então verifico que as três mídias foram enviadas e estão sendo exibidas de forma consecutiva

******************************************************************************************************************************************
******************************************************************************************************************************************

Cenário 9: Verificar envio de um áudio para um grupo

Dado que estou na página de diálogo de conversas de um grupo
Quando aperto o botão do microfone
    E seguro por 5 segundos 
    E durante este tempo corrido de 5 segundo falo “Hello World”
    E em seguida deixo de pressionar o ícone
Então visualizo que um retângulo é exibido
    E dentro do retângulo a foto do contato contendo o tempo de gravação
    E logo ao lado um ícone de play
    E a hora que a mensagem foi gravada
	E o a flag de mensagem enviada
	
******************************************************************************************************************************************
******************************************************************************************************************************************

Cenário 10: Verificar o encaminhamento de um áudio para contato 

Dado que possuo dois contatos no aplicativo
	E estou na página de diálogo de conversas do primeiro contato
	E já possuo um áudio enviado
Quando pressiono a mensagem de áudio
	E seleciono a opção de encaminhar
	E em seguida pressiono a seta da esquerda que aparece na parte inferior do aplicativo
	E seleciono o segundo contato
Então verifico que a mensagem foi encaminhada para o contato no formato de um retângulo
	E dentro do retângulo a foto do contato contendo o tempo de gravação
    E logo ao lado um ícone de play
    E a hora que a mensagem foi gravada
	E o a flag de mensagem enviada

******************************************************************************************************************************************
******************************************************************************************************************************************

Cenário 11: Visualizar mensagem de erro para mensagens com menos de 1 segundo

Dado que estou na tela de diálogo de mensagens
Quando pressiono o botão do microfone por menos de 1 segundo
Então ouço o som de gravação de mensagem 
	E visualizo um balão de mensagem na cor cinza
	E dentro a mensagem de "pressione para gravar, solte para enviar”
	E no topo da mensagem um "X" para fechar a mensagem
	
******************************************************************************************************************************************
******************************************************************************************************************************************

Cenário 12: Fechar mensagem de erro para mensagens com menos de 1 segundo

Dado que executei o cenário 11
Quando pressiono o "X" para fechar a mensagem	
Então vejo que a mensagem foi fechada
	E a tela de diálogo é exibida novamente
